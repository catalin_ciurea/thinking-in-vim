"----------------------------------------------------------
" Options
"----------------------------------------------------------
"set scrollbind
"au! BufWritePost .vimrc source %
"let g:perl_next_method_disable = 1
let VCSCommandDisableAll=1
syntax on
set termencoding=utf-8
set encoding=utf-8
set nowrapscan
set expandtab
set tabstop=4
set softtabstop=4
set laststatus=2
set statusline=%F%m%r%h%w%q\ [BUF=%n]\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [CHAR=%04b\ HEX=0x%04B]\ [POS=%04l,%04v][%p%%]\ [LEN=%L]
set backup
set writebackup
set backupcopy=yes
set backupext=.bak
"set patchmode=.orig
set backupdir=~/.vimbackup
set undodir=~/.vimundo
set undofile
set undolevels=5000
set undoreload=50000
set splitbelow
set splitright
set mouse=a
set ignorecase 
set smartcase
set infercase
set autoindent
set cindent
set clipboard=unnamed
"set smartindent
set matchpairs+=<:>
set path+=**,~/,~/work/**
set acd
set browsedir=current
set diffopt=vertical,iwhite
set incsearch
set autoread
set backspace=indent,eol,start
set wildignore=*.bak,*.o,*.e,*~,*.orig
set wildmenu
set hlsearch
set showcmd		" display incomplete commands
set complete+=k
set shiftwidth=4
set showmatch
set nocompatible
set history=100
set viminfo='1000,f1,<500
set ruler
set nrformats=
set listchars=tab:>.,eol:$,trail:_,extends:>,precedes:<
" Set up ack as the main grep program
set grepprg=/pkg/perl-5.14.1/bin/ack\ --H\ --nogroup\ --column\ $*
set grepformat=%f:%l:%c:%m
set autoread
" set spell
" set spelllang=ro

filetype indent on
"set showmode
"set list
"set previewheight=17

filetype plugin indent on
runtime! ftplugin/man.vim
let g:netrw_keepdir=0
highlight MatchParen ctermbg=blue guibg=lightyellow
au FileType perl,python,tcl set commentstring=#\ %s
au FileType c,cpp,java,perl set mps+==:;
au FileType text setlocal textwidth=78

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

"helptags /home/users/ciureac/.vim/doc/
"set updatetime=2000
"autocmd BufEnter * lcd %:p:h
"----------------------------------
" Abbreviations
"----------------------------------
ab bsh  #!/bin/bash
ab pl58  #!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

ab pl514 #!/pkg/perl-5.14.1/bin/perl

use 5.14.1;
use warnings;
use Data::Dumper;
use Data::Printer;
use Data::Show;
use Getopt::Lucid qw(:all);

ab envpl #!/usr/bin/env perl
ab vimmod # VIM modeline
# vim:tw=78:ai:si:sm:ts=4:is:wmnu:sw=4:hls:sc:acd:et:ar:sb:spr:bex=.bak:mouse=a:fdm=marker
ab #= #============================================================
ab teh the
ab myself my $self = shift;
"
"
"----------------------------------
" Mappings
"----------------------------------
"
"noremap   <silent> <F4>         :execute ":ptag ".expand("<cword>")<CR>
"inoremap  <silent> <F4>    <C-C>:execute ":ptag ".expand("<cword>")<CR>
"noremap   <silent> <F6>         :copen<CR>
"noremap   <silent> <F7>         :cclose<CR>
"noremap   <silent> <F8>         :cprevious<CR>
"noremap   <silent> <F9>         :cnext<CR>
"noremap   <silent> <F11>        :TlistToggle<CR>
"noremap   <silent> <F12>        :TlistClose<CR>

" surround visual selection with an if block
map \f iif (
map \u iunless (
map \o ifor (
map \w iwhile (
map \b isub 


inoremap  , ,<Space>
inoremap  (  ()<Left>
inoremap  [  []<Left>
inoremap  {  {}<Left>

" additional third party stuff
map \l :noh<CR>
noremap \tg :!ctags *pm<CR>
" autocmd BufWritePost * call system("ctags *")
"
" Prompt to open file with same name, different extension
map \er :e <C-R>=expand("%:r")."."<CR>

"-------------------------------------------------------------------------------
" Fast switching between buffers
" The current buffer will be saved before switching to the next one.
" Choose :bprevious or :bnext
"-------------------------------------------------------------------------------
 noremap  <silent> <s-tab>       :if &modifiable && !&readonly &&
     \                      &modified <CR> :write<CR> :endif<CR>:bprevious<CR>
inoremap  <silent> <s-tab>  <C-C>:if &modifiable && !&readonly &&
     \                      &modified <CR> :write<CR> :endif<CR>:bprevious<CR>

map <silent> \gm :call Go_to_middle_of_line()<CR>

function! Go_to_middle_of_line()
    let line_length = col("$")
    let middle_position = line_length / 2
    call cursor(".", middle_position)
endfun

if exists(":Tabularize")
    nmap \a= :Tabularize /=<CR>
    vmap \a= :Tabularize /=<CR>
    nmap \a: :Tabularize /:\zs<CR>
    vmap \a: :Tabularize /:\zs<CR>
endif

inoremap <silent> <Bar>   <Bar><Esc>:call <SID>align()<CR>a

function! s:align()
    let p = '^\s*|\s.*\s|\s*$'
    if exists(':Tabularize') && getline('.') =~# '^\s*|' && (getline(line('.')-1) =~# p || getline(line('.')+1) =~# p)
        let column = strlen(substitute(getline('.')[0:col('.')],'[^|]','','g'))
        let position = strlen(matchstr(getline('.')[0:col('.')],'.*|\s*\zs.*'))
        Tabularize/|/l1
        normal! 0
        call search(repeat('[^|]*|',column).'\s\{-\}'.repeat('.',position),'ce',line('.'))
    endif
endfunction

" let g:pattern_method = { 'perl' : '\v^\s*sub\s*\w+\s*(\(\s*.{-}\s*\))?\s*\{*' }
" function! g:Next_method()
"      call search(g:pattern_method['perl'], 'W')
" endfunction

" function! g:Previous_method()
"      call search(g:pattern_method['perl'], 'bW')
" endfunction

" au FileType perl map <silent> ]m :call search(g:pattern_method['perl'], 'W')<CR>
" au FileType perl map <silent> [m :call search(g:pattern_method['perl'], 'bW')<CR>
" map <silent> ]m :call g:Next_method()<CR>
" map <silent> [m :call g:Previous_method()<CR>
"
" omap O <Esc>:call MyMotion(v:operator)<CR>

" ======================


" au FileType perl noremap <silent> ]m :<C-U>call Perl_method_jump('')<CR>
" au FileType perl noremap <silent> [m :<C-U>call Perl_method_jump('b')<CR>
" au FileType perl noremap <silent> ]M :<C-U>call Perl_method_end_jump()<CR>
" au FileType perl noremap <silent> [M :<C-U>call Perl_method_jump_before()<CR>

function! Perl_method_jump(type) range
    let l:pattern =  '\v^\s*sub\s*\w+\s*(\(\s*.{-}\s*\))?\s*\n*\{'
    for i in range(1, v:count1)
        call search(l:pattern, a:type . 'W')
    endfor
endfunction

function! Perl_method_jump_before() range
    " let a:lastline -= 1
    let l:pattern =  '\v^\s*sub\s*\w+\s*(\(\s*.{-}\s*\))?\s*\n*\{'
    " retrieve the cursor position
    let l:current_pos = getpos(".")
    " keep the counter as it is going to be reset by any normal mode command
    let l:counter = v:count1

    if !(searchpos(l:pattern, 'bceW')[0])
        " no previous subroutine so nothing to do
        return 1
    endif

    " keep this flag to determine if we have any previous method to jump to
    let l:have_previous_method = search(l:pattern, 'nbeW')

    keepjumps normal %

    if (!l:have_previous_method)
        if (getpos(".")[1] >= l:current_pos[1])
            call setpos(".", l:current_pos)
        endif
        return 1
    endif

    if (getpos(".")[1] >= l:current_pos[1])
        let l:counter +=1
    endif

    call Jump_to_nr_of_sub_end(l:counter, l:pattern, 'b')

endfunction


function! Perl_method_end_jump() range
    let l:pattern =  '\v^\s*sub\s*\w+\s*(\(\s*.{-}\s*\))?\s*\n*\{'
    " retrieve the cursor position
    let l:current_pos = getpos(".")
    " keep the counter as it is going to be reset by any normal mode command
    let l:counter = v:count1

    " search backwards first to see if there are any subroutines before cursor
    if !(searchpos(l:pattern, 'bceW')[0])
        " no previous subroutine so jump forward
        call Jump_to_nr_of_sub_end(l:counter, l:pattern, '')
        return 1
    endif

    " keep this flag to determine if we have any succesive methods
    let l:have_next_method = search(l:pattern, 'neW')
    " jump to the end of the previous subroutine
    keepjumps normal %

    if (!l:have_next_method)
        if (getpos(".")[1] <= l:current_pos[1])
            call setpos(".", l:current_pos)
        endif
        return 1
    endif

    if (getpos(".")[1] > l:current_pos[1])
        let l:counter -=1
    endif

    " we were not in a subroutine so we simply jump forward
    call Jump_to_nr_of_sub_end(l:counter, l:pattern, '')
endfunction

function! Jump_to_nr_of_sub_end(jumps, sub_pattern, type)
    " if the count is higher than the nr of possible jumps
    " we jump the max nr we can (default behaviour of ]m)
    let l:pos_moved = 0
    for i in range(1, a:jumps)
        if (search(a:sub_pattern, a:type . 'eW')) " use 'e' flag to jump to the end of match. Important !!
            let l:pos_moved += 1
        endif
    endfor
    " if the value was incremented it means we jumped
    " somewhere on '{' in a 'sub foo {' so we call '%' to 
    " jump to mathcing brace
    if (l:pos_moved)
        keepjumps normal %
    endif
endfunction

xnoremap * :<C-u>call <SID>VSetSearch()<CR>/<C-R>=@/<CR><CR>
xnoremap # :<C-u>call <SID>VSetSearch()<CR>?<C-R>=@/<CR><CR>
function! s:VSetSearch()
    let temp = @s
    norm! gv"sy
    let @/ = '\V' . substitute(escape(@s, '/\'), '\n', '\\n', 'g')
    let @s = temp
endfunction

augroup filetype
  au BufRead,BufNewFile *.flex,*.jflex    set filetype=jflex
augroup END
au Syntax jflex    so ~/.vim/syntax/jflex.vim

fun! SetDiffColors()
     highlight   DiffAdd      cterm=bold   ctermfg=white   ctermbg=DarkGreen
     highlight   DiffDelete   cterm=bold   ctermfg=white   ctermbg=DarkGrey
     highlight   DiffChange   cterm=bold   ctermfg=white   ctermbg=DarkBlue
     highlight   DiffText     cterm=bold   ctermfg=white   ctermbg=DarkRed
endfun
autocmd FilterWritePre * call SetDiffColors()
